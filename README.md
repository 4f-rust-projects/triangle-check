# Triangle check

# Overall
From 3 vertex lengths which inputed by you as *a, b, c*, the program will check if it was a triangle then show you which type of triangle and its perimeter and area in output.

# Test the program

First of all, you must install **Rust** first. After that, you have to clone this repository to your local hard drive.

Run:

``` cd triangle-check && cargo run```

Have fun!

